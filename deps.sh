#!/usr/bin/env bash
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

set -e

sudo dnf install -y \
    texlive \
    texlive-scheme-basic \
    texlive-changepage \
    texlive-gillius
