# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

PDF := build/resume.pdf
VIEWER := evince

default: $(PDF)

$(PDF): resume.tex
	mkdir -p build/
	pdflatex -halt-on-error -output-directory=build $<

.PHONY: run
run: $(PDF)
	$(VIEWER) $<

.PHONY: clean
clean:
	$(RM) -r build/
