<!-- SPDX-License-Identifier: CC-BY-ND-4.0 -->
<!-- SPDX-FileCopyrightText: 2024 Tim Crawford <crawfxrd@proton.me> -->

# Tim Crawford

- [Codeberg](https://codeberg.org/tcrawford)
- [GitHub](https://github.com/crawfxrd)

## Skills

- **Languages**: Rust, C, Python, Bash
- **Tools**: Git, CMake, QEMU, GitLab, GitHub

## Experience

### System76 (Denver, CO)

**Firmware Developer**: October 2019 - Present

- Development of System76 Open Firmware (C, Rust)
  - coreboot bootloader, edk2 payload, and Rust UEFI modules
- Development of System76 Embedded Controller (C)
  - Firmware for ITE's 8051-based embedded controllers used in laptops
- Occasionally upstreaming patches to the Linux kernel

### Datto, Inc (Norwalk, CT)

**Software Engineer I**: July 2019 - August 2019

- Development of Cloud Continuity agent for Windows (C++)

**QA Engineer**: February 2017 - July 2019

- Maintainer of open source **dattobd** Linux kernel driver (C)
  - Fixed issues with new kernel releases while maintaining compatibility with
    old kernels.
  - Added new features as required for Datto backup service.
  - Addressed issues reported by community through GitHub.
- Modernized CMake-based build system for Linux and macOS
- Automated generation of Windows images for CI using Packer and OpenStack
- Wrote automated tests (Bash) for ZFS encryption, most included in ZFS test suite

**Associate QA Engineer**: February 2015 - February 2017

- Wrote automated tests (Bash, Python) for Linux and macOS backup agents
- Setup Buildbot CI instance for building Linux backup solution on supported distros

## Projects

### [System76 Embedded Controller](https://github.com/system76/ec)

**Work project**: 2020-present

Open-source firmware for the embedded controllers in System76 laptops.

### [System76 Open Firmware](https://github.com/system76/firmware-open)

**Work project**: 2020-present

An open-source firmware distribution of coreboot, edk2, and Rust-based UEFI
applications and drivers for System76 laptops.

### [dattobd](https://github.com/datto/dattobd)

**Work project**: 2016-2019

A Linux kernel module for creating a snapshot of a live (mounted) volume.
Ensures data consistency for backup and physical-to-virtual solutions built on
top of it.


### [Ping network utility](https://codeberg.org/tcrawford/ping)

**Personal project**: 2015

Developed an implementation of Microsoft's ping command for Windows Server 2003
in C using the Windows API. A modified version was accepted by
[ReactOS](https://reactos.org) to replace their existing implementation.

## Education

### Bachelor of Science, Computer Science (May 2014)

State University of New York at New Paltz
